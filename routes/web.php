<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
* Корневой маршрут
*/
Route::get('/', function () {
    return view('machines.index');
});

/**
* Маршрут к ресурс контроллеру машин
*/
Route::resource('/machine', 'Machine\MachineController', ['only' => ['store']]);
