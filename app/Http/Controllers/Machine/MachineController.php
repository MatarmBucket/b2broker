<?php

namespace App\Http\Controllers\Machine;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MachineRequest as MachineFormRequest;

class MachineController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MachineFormRequest $request)
    {
        return response()->json(['status' => 'ok', 'time'=> date('Y-m-d H:i:s')]);
    }
}
