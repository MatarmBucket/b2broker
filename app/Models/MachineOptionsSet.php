<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MachineOptionsSet extends Model
{
    protected $table = 'machines_options_set';

    public $timestamps = false;
}
