<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MachineOption extends Model
{
    protected $table = 'machines_options';

    public $timestamps = false;
}
