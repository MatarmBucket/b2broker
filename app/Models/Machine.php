<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    protected $table = 'machines';

    public $timestamps = false;

    /**
     * Вернет связь с моделью MachineOption
     */
    public function machineOption()
    {
        return $this->hasOne(MachineOption::class, 'id', 'machine_id');
    }

    /**
     * Вернет связь с моделью MachineOptionSet
     */
    public function machineOptionsSet()
    {
        return $this->hasOne(MachineOptionSet::class, 'id', 'machine_id');
    }
}
