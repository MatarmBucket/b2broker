<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include ('partials.meta')
    </head>
    <body>
        <div id="app">
            <div class="navbar navbar-inverse bg-inverse">
                <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand">Решение тестового задания для B2Broker.</a>
                <a class="d-flex ml-3" href="{{ asset('storage/phpProgrammerTest.pdf') }}">PDF файл с заданием</a>
                </div>
            </div>

            <section class="jumbotron">
                <div class="container">
                  @yield('content')
                </div>
            </section>
        </div>
        @include('partials.scripts')
    </body>
</html>
