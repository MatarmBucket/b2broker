<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines_options', function (Blueprint $table) {
            $table->integer('machine_id')->unsigned();
            $table->string('firmware', 32);
            $table->integer('connect_freq');
            $table->unique('machine_id', 'machines_options_machine_id_index');
            $table->foreign('machine_id')->references('id')->on('machines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines_options');
    }
}
