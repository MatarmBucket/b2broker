<?php

use Illuminate\Database\Seeder;

use \App\Models\{Machine, MachineOption};

class MachinesSeeder extends Seeder
{
    private $permanentMachines = [
        [
            'serial' => 123456789012345,
            'firmware' => '1.01a',
            'connect_freq' => 10
        ],
        [
            'serial' => 123456789012347,
            'firmware' => '1.02a',
            'connect_freq' => 8
        ]
    ];

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        // Создаем заданные машины
        foreach ($this->permanentMachines as $key => $options) {
            $this->createMachinesAndOptions($options);
        }
    }

    /**
     * Создает машину с опциями
     * @param $options array - массив параметров машины
     * @return void
     */
    private function createMachinesAndOptions(array $options)
    {
        $machine = factory(Machine::class)->create([
                'serial' => $options['serial']
            ]);

        factory(MachineOption::class)->create([
                'machine_id' => $machine->id,
                'firmware' => $options['firmware'],
                'connect_freq' => $options['connect_freq']
            ]);
    }
}
