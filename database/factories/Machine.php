<?php

use Faker\Generator as Faker;
use App\Models\Machine;

$factory->define(Machine::class, function (Faker $faker) {
    return [
        'serial' => 123456789012345
    ];
});
