<?php

use Faker\Generator as Faker;
use App\Models\MachineOption;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(MachineOption::class, function (Faker $faker) {
    return [
        'machine_id' => 0,
        'firmware' => '1.01a',
        'connect_freq' => 10
    ];
});
